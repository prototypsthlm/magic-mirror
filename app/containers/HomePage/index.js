import React from 'react';
import Webcam from 'react-webcam';
import CountDown from './CountDown';
import face from '../../../facerecognition/recognize';
import VoiceRecognition from '../../../voice/recognize';
import Speech from '../../../voice/speak';
import slack from '../../../slack/slack';

const enroll = require('../../../enroll/enroll');


export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    constructor(props) {
        super(props);
        this.state = {screenMessage: '', isListening: '', isSpeaking: ''};
        this.speech = new Speech((state) => this.setState({isSpeaking: state}));
        this.voiceRecogition = new VoiceRecognition((state) => this.setState({isListening: state}));
    }

    setRef = (webcam) => {
        this.webcam = webcam;
    };

    //noinspection JSUnusedGlobalSymbols
    componentDidMount = () => this.mainFlow();

    mainFlow() {
        console.log('Starting main flow');
        this.voiceRecogition.startListening(this.onCommandFound);
    }

    say = (message) => {
        console.log(`  Say: ${message}`);
        this.setState({screenMessage: message});
        setTimeout(() => this.setState({screenMessage: ''}), 5000);

        return this.speech.speak(message);
    };

    onCommandFound = (command) => {
        this.captureImage()
                .then((faceResponse) => {
                    return this.handleFaceResponse(faceResponse);
                })
                .then(() => {
                    console.log('Done');
                    //this.voiceRecogition.forceStop();
                    //this.mainFlow();
                })
                .catch((err) => {
                            console.error(err);
                            this.mainFlow();
                        }
                );
    };

    captureImage = () => {
        return face.recognizeFace(this.webcam.getScreenshot());
    };

    handleFaceResponse = (faceResponse) => {
        if (faceResponse.images[0].candidates)
            return this.foundCandidate(faceResponse);
        else
            return this.say('Vi har inte träffats förrut.');
        //return this.candidateNotFound(faceResponse);
    };

    foundCandidate = (data) => {
        const subjectId = data.images[0].candidates[0].subject_id;
        const candidateName = enroll.lookUpFirstName(subjectId);
        slack(`${candidateName} har anlänt`);

        return this.say(`Hej ${candidateName}`);
    };

    candidateNotFound = (faceResponse) => {
        return this.say('Hej okänd, skulle du vilja registrera dig?')
                .then(() => {
                    return new Promise((resolve, reject) => {
                        this.voiceRecogition
                                .startListening(['ja', 'nej'])
                                .then((answer) => {
                                    if (answer.match(/.*ja.*/)) {
                                        return this.enrollNewUser(faceResponse);
                                    } else if (answer === 'nej') {
                                        console.log('Nej. Startar om');
                                    } else {
                                        console.log('Jag förstod inte vad du sa, svara med ja eller nej');
                                    }
                                    return resolve();
                                }, reject)
                    });
                }, (err) => {
                    return Promise.reject(err)
                });
    };

    enrollNewUser = (faceResponse) => {
        return new Promise((resolve, reject) => {
                    this.say("Vad heter du?")
                            .then(this.voiceRecogition.startListening([]), reject)
                            .then((answer) => {
                                if (answer) {
                                    return enroll.enrollPerson({subject_id: answer, image: faceResponse.uploaded_image_url});
                                } else {
                                    reject;
                                }
                            }, reject)
                }
        );
    };

    render() {
        return (
                <div- className="container">
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <h1 className="screenmessage">{this.state.screenMessage}</h1>
                            <div>
                                <Webcam audio={false} width={"400px"} height={"auto;"} screenshotFormat="image/jpeg"
                                        ref={this.setRef}/>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <CountDown afterCountdown={this.captureImage}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <span className={'listening ' + (this.state.isListening ? '' : 'hide')}>Lyssnar</span>
                            <span className={'speaking ' + (this.state.isSpeaking ? '' : 'hide')}>Pratar</span>
                        </div>
                    </div>
                </div->
        );
    }
}

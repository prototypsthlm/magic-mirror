import React from 'react';

export default class CountDown extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super();
        this.props = props;
        this.state = { time: {}, seconds: 3, started: false };
        this.timer = 0;
        this.startTimer = this.startTimer.bind(this);
        this.countDown = this.countDown.bind(this);
    }

    secondsToTime(secs){
        let hours = Math.floor(secs / (60 * 60));

        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);

        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);

        let obj = {
            "h": hours,
            "m": minutes,
            "s": seconds
        };
        return obj;
    }

    componentDidMount() {
        let timeLeftVar = this.secondsToTime(this.state.seconds);
        this.setState({ time: timeLeftVar });
    }

    startTimer() {
        if (this.timer == 0) {
            this.setState({started: true});
            this.timer = setInterval(this.countDown, 1000);
        }
    }

    countDown() {
        // Remove one second, set state so a re-render happens.
        let seconds = this.state.seconds - 1;
        this.setState({
            time: this.secondsToTime(seconds),
            seconds: seconds,
        });

        // Check if we're at zero.
        if (seconds == 0) {
            this.props.afterCountdown();
            clearInterval(this.timer);
        }
    }

    render() {
        let seconds = this.state.started ? this.state.seconds : '';
        return(
            <div>
                <button className="btn-primary" onClick={this.startTimer}>Start</button>
                <h1>{seconds}</h1>
            </div>
        );
    }
}

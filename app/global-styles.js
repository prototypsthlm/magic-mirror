import {injectGlobal} from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }
  
  .container{
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: black;
    min-height: 100%;
    min-width: 100%;
  }
  
  .screenmessage{
      margin-top: 200px;
      min-height:45px;
      display:inline-block;
      padding:10px;
  }

  .speaking {
      min-height:45px;
      padding:10px;
      color: red;
  }

  .listening {
      min-height:45px;
      padding:10px;
      color: green;
  }
  
  .hide {
    color: black;
  }
  
  h1, h2, h3, h4, h5, h6, h7, span, p{
    color: white;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
`;

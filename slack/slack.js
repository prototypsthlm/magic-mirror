const fetch = require('node-fetch');

function sendToSlack(message) {
  return new Promise((resolve, reject) => {
    fetch('https://hooks.slack.com/services/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', {
      method: 'POST',
      body: `{"text": "${message}"}`,
    }).then((res) => {
      resolve(res.status);
    }).catch((err) => {
      console.error(`Error when calling slack: ${res.status} + ${err}`);
      reject(err.status);
    });
  });
}

module.exports = sendToSlack;

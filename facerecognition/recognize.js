const fetch = require('node-fetch');
const config = require('../config');
const enrollment = require('../enroll/enrollment');

function recognizeFace(imageSrc) {
    const data = {
        image: imageSrc,
        gallery_name: enrollment.gallery_name,
    };

    return new Promise((resolve, reject) => {
        fetch('https://api.kairos.com/recognize', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                app_id: config.kairo.app_id,
                app_key: config.kairo.app_key,
            },
            body: JSON.stringify(data),

        }).then((response) => response.json())
                .then((data) => {
                    if (data.images) {
                        console.log(`  Uploaded image: ${data.uploaded_image_url}`);
                        resolve(data);
                    }
                }).catch(reject);
    });
}


module.exports.recognizeFace = recognizeFace;
const SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
const SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
const SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

class VoiceRecognition {

    constructor(callback) {
        this.recognition = new SpeechRecognition();
        this.setIsListening = callback;
    }

    forceStop = () => {
        this.shouldForceStop = true;
        this.recognition.stop();
    }

    startListening = (commandFound) => {
        const self = this;
        this.recognition.grammars = new SpeechGrammarList();
        this.recognition.lang = 'sv-SE';
        this.recognition.interimResults = false;
        this.recognition.maxAlternatives = 1;

        this.startRecognition();

        this.recognition.onstart = () => {
            this.setIsListening(true);
            console.log(`  Recognition start`);
        };

        this.recognition.onend = () => {
            this.setIsListening(false);
            console.log('  Recognition end');

            if (!this.shouldForceStop) {
                self.recognition.start();
            }
        };

        this.recognition.onerror = (event) => {
            console.log(`  Error detected: ${event.error}`);
        };

        this.recognition.onresult = (event) => {
            console.log('    Got event');

            if (event.results.length > 0) {
                //console.log(event);
                const command = event.results[0][0].transcript;
                console.log(`  Command: ${command}`);
                commandFound(command);
            } else {
                console.log('No matching result');
            }
        };
    };

    startRecognition() {
        try {
            this.recognition.start();
        } catch (exception) {
            console.log('  Was already started');
        }
    }
}

export {VoiceRecognition as default};

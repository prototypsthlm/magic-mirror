class Speech {
    constructor(callback) {
        this.setIsSpeaking = callback;
    }

    speak(words) {
        return new Promise((resolve, reject) => {
            if (words && words.length > 0) {
                const utterance = new SpeechSynthesisUtterance(words);
                utterance.lang = 'sv-SE';
                utterance.rate = 1.0;

                utterance.onerror = reject;
                utterance.onstart = () => this.setIsSpeaking(true);
                utterance.onend = () => {
                    this.setIsSpeaking(false);
                    resolve();
                };
                speechSynthesis.speak(utterance);
            }
            resolve();
        });
    }
}

export {Speech as default}
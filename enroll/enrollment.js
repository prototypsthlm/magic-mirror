module.exports = {
    gallery_name: 'Prototyp - Employees',
    persons: [
        {
            id: 'Bjorn Helgesson',
            name: 'Björn Helgesson',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_BJ%C3%96RN_WEBL.jpg',
        },
        {
            id: 'Jacob Hallenborg',
            name: 'Jacob Hallenborg',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_JACOB_H_WEBL.jpg',
        },
        {
            id: 'Fredrik Hasselgren',
            name: 'Fredrik Hasselgren',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_FREDRIK_WEBL.jpg',
        },
        {
            id: 'Christopher Laursen',
            name: 'Christopher Laursen',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_CHRISTOPHER_WEBL.jpg',
        },
        {
            id: 'Erik Lindgren',
            name: 'Erik Lindgren',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_ERIK_WEBL.jpg',
        },
        {
            id: 'Gustav Rannestig',
            name: 'Gustav Rannestig',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_GUSTAV_WEBL.jpg',
        },
        {
            id: 'Isaac Rondon',
            name: 'Isaac Rondon',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_ISAAC_WEBL.jpg',
        },
        {
            id: 'Jacob Broms',
            name: 'Jacob Broms',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_JACOB_WEBL.jpg',
        },
        {
            id: 'Jonna Karlsson Sellen',
            name: 'Jonna Karlsson Sellén',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_JONNA_WEBL.jpg',
        },
        {
            id: 'Pontus Osterberg',
            name: 'Pontus Österberg',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_PONTUS_WEBL.jpg',
        },
        {
            id: 'Stefan Larsson',
            name: 'Stefan Larsson',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_STEFAN_WEBL.jpg',
        },
        {
            id: 'Tobias Andersson',
            name: 'Tobias Andersson',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_TOBIAS_A_WEBL.jpg',
        },
        {
            id: 'Tobias Bard',
            name: 'Tobias Bard',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_TOBIAS_B_WEBL.jpg',
        },
        {
            id: 'Tobias Rundbom',
            name: 'Tobias Rundbom',
            image: 'http://www.prototyp.se/assets/imgs/medarbetare/PROTOTYP_TOBIAS_R_WEBL.jpg',
        },
    ],
};

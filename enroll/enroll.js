import fetch from "node-fetch";
import config from "../config";
import enrollment from "../enroll/enrollment";

export function enrollPerson(person) {

    console.log('Enrolling: ');
    console.log(person);

    const data = {
        gallery_name: enrollment.gallery_name,
        subject_id: person.id,
        image: person.image,
    };

    return new Promise((resolve, reject) => {
        fetch('https://api.kairos.com/enroll',
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        app_id: config.kairo.app_id,
                        app_key: config.kairo.app_key,
                    },

                    body: JSON.stringify(data),
                })
                .then((res) => {
                    if (res.status === 200) {
                        res.json().then((body) => {
                            if (body.Errors) {
                                reject(`Failed to enroll ${person.id}\n${body}`);
                            } else {
                                resolve(`Enrolled ${person.name}`);
                            }
                        });
                    }
                })
                .catch(reject);

    });
}

function enrollAll() {
    enrollment.persons.forEach((person) => enrollPerson);
}

export function lookUpFirstName(subjectId) {
    console.log(`    Looking up ${subjectId}`);
    const person = enrollment.persons.find((p) => p.id === subjectId);
    if (person) {
        return person.name.split(/ /)[0];
    }

    console.error(`Could not find person by id '${subjectId}'`);
    return subjectId.split(/ /)[0];
}

